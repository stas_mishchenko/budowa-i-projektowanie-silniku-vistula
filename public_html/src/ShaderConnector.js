//Strict mode: variables must be declared before used
"use strict";

//OurShader 
var globalShader = null;
//attribute for the Vertex
var globalVertexPosAttr = null;


function initShader(vertexShaderID, fragmentShaderID) {
    // 1. load and compile vertex and fragment shaders
    var vertexShader = CompileShader(vertexShaderID, globalGL.VERTEX_SHADER);
    var fragmentShader = CompileShader(fragmentShaderID, globalGL.FRAGMENT_SHADER);

    // 2. Create and link the shaders into a program.
    globalShader = globalGL.createProgram();
    globalGL.attachShader(globalShader, vertexShader);
    globalGL.attachShader(globalShader, fragmentShader);
    globalGL.linkProgram(globalShader);

    // 3. check for error
    if (!globalGL.getProgramParameter(globalShader, globalGL.LINK_STATUS)) {
        alert("Error linking shader");
    }

    // 4. Gets a reference to the Vertex Postion variable within the shaders.
    globalVertexPosAttr = globalGL.getAttribLocation(globalShader, "aVertexPos");
        // aVertexPos: is defined in the VertexShader (in the index file)

    // 5. Activates the vertex buffer loaded in VertexBuff.js
    globalGL.bindBuffer(globalGL.ARRAY_BUFFER, globalVertexB);
        // globalVertexB: is defined in VertexBuff.js 

    // 6. Describe the characteristic of the vertex position attribute
    globalGL.vertexAttribPointer(globalVertexPosAttr, // variable initialized above
        3,          // each vertex element is a 3-float (x,y,z)
        globalGL.FLOAT,  // data type is FLOAT
        false,      // if the content is normalized vectors
        0,          // number of bytes to skip in between elements
        0);         // offsets to the first element
}

//funkcja dla indexu
function CompileShader(id, shaderType)
{
    var shaderText, shaderSource, compiledShader;
    shaderText= document.getElementById(id);
    shaderSource = shaderText.firstChild.textContent;
    
    //vertex albo fragment
    compiledShader=globalGL.createShader(shaderType);
    
    //compile
    globalGL.shaderSource(compiledShader, shaderSource);
    globalGL.compileShader(compiledShader);
    
    //errory!!!
    if (!globalGL.getShaderParameter(compiledShader, globalGL.COMPILE_STATUS))
    {
        alert ("Compilacja Shadera ERROR: " + globalGL.getShaderInfoLog(compiledShader));
    }
    return compiledShader;
}

